```

  _                             _______             
 | |                           |__   __| /\     _   
 | |      ___   __ _  _ __  _ __  | |   /  \  _| |_ 
 | |     / _ \ / _` || '__|| '_ \ | |  / /\ \|_   _|
 | |____|  __/| (_| || |   | | | || | / ____ \ |_|  
 |______|\___| \__,_||_|   |_| |_||_|/_/    \_\     
                                                    
                                                    

```


![Java](https://img.shields.io/badge/java-%23ED8B00.svg?style=for-the-badge&logo=openjdk&logoColor=white)
![Apache Maven](https://img.shields.io/badge/Apache%20Maven-C71A36?style=for-the-badge&logo=Apache%20Maven&logoColor=white)
![Static Badge](https://img.shields.io/badge/Licence-Affero_GPL3-blue)

🚧 In development 🚧

## Description

LearnTA+ is a passive automata learning algorithm to automatically construct abstract models (Timed Automata) from observations (execution traces) of real systems.

## Table of Contents
- [Technologies](#Technologies)
- [Installation](#installation)
- [Usage](#usage)

## Technologies
LearnTA+ depends on:
- <a href="https://www.java.com/es/" target="_blank">Java (JDK 17 or higher)</a>
- <a href="https://github.com/FasterXML/jackson" target="_blank">Jackson</a>
- <a href="https://github.com/FasterXML/jackson-modules-base/tree/2.18/blackbird" target="_blank">Jackson Blackbird module</a>
- <a href="https://github.com/nidi3/graphviz-java" target="_blank">Graphviz Java</a>

## Installation

### From source code
Pre-requisite: Java Development Kit (JDK)  17 or higher, and Maven.

LearnTA+ can be used from source code, using Maven.

### From binary
Pre-requisite: Java Development Kit (JDK) 17 or higher.

LearnTA+ binary (bin folder) can be imported as a library.


## Usage

### Traces format
The input traces of LearnTA have the form of:
```json
[
  {
    "observations": [ // Every trace starts is a list of observations
      { // Each observation has 3 fields:
        "time": 0, // Elapsed time from the start of the execution
        "event": "", // String that collects if an event have happened
        "variables": [  // A list of system attributes
          "close"
        ]
      },
      {
        "time": 2,
        "event": "",
        "variables": [
          "close"
        ]
      }
    ]
  },
  {
    "observations": [
      {
        "time": 0,
        "event": "pushed?",
        "variables": [
          "close"
        ]
      },
      {
        "time": 2,
        "event": "",
        "variables": [
          "close"
        ]
      }
    ]
  }
]
```

There are more examples of traces in "src/main/resources".

### Create a new LearnTA class.

Create a LearnTA class using the two possible constructors. And learn an automaton from the traces.
```java
import learning_algorithm.LearnTA;

LearnTA la = new LearnTA(String "path to traces", int k);
LearnTA la = new LearnTA(ArrayList<Trace> traces, int k);
```

### Get and visualize the automaton
```java
import automaton.Automaton;

String traces = "door.json";
// Create new LearnTA class
LearnTA la = new LearnTA("src/main/resources/"+traces, 2);
//Get automaton
Automaton a = la.getAutomaton();
// Visualize the automaton in web browser
LearnTA.show(a);
```


### Generalize the learned automaton
You can transform your automata into a deadlock-free version taking the type of events (input/ output) into account:

```java
import generalized_automaton.GenAutomaton;

// Generalize
GenAutomaton ga = la.generalize();

LearnTA.show(ga);
```

### Verification module
```java
import validator.Validator;

String traces = "door.json";
// Create new LearnTA class
LearnTA la = new LearnTA("src/main/resources/"+traces, 2);

Automaton a = la.getAutomaton();
GenAutomaton ga = la.generalize();

Validator.nValidTraces(ArrayList<Trace> traces, a);
Validator.nValidTraces(ArrayList<Trace> traces, ga);
```

### Other Functionalities

#### Import / Export Automata
```java
// Import
Automaton b = Automaton.importFromDOT("Automaton.dot");
// Export
la.exportDOT("Automaton");
la.exportPNG("Automaton2");
```

### Create a list of traces from JSON

```java
import learning_algorithm.LearnTA;

ArrayList<Trace> newTraces = LearnTA.readTraces("src/main/resources/"+traces);
```
