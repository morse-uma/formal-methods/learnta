import automaton.Automaton;
import generalized_automaton.GenAutomaton;
import learning_algorithm.LearnTA;


public class Main {

	public static void main(String[] args) {
		
		try {
			String traces = "door.json";
			// Create new LearnTA class
			LearnTA la = new LearnTA("src/main/resources/"+traces, 2);
			// Get automaton
			Automaton a = la.getAutomaton();
			// Show
			LearnTA.show(a);
			// Generalize
			GenAutomaton ga = la.generalize();
			LearnTA.show(ga);

			// Import
			Automaton b = Automaton.importFromDOT("Automaton.dot");
			// Export
			la.exportDOT("Automaton");
			la.exportPNG("Automaton2");


			System.out.println("                              ");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
