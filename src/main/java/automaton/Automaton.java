package automaton;

import guru.nidi.graphviz.attribute.Color;
import guru.nidi.graphviz.attribute.Label;
import guru.nidi.graphviz.attribute.Style;
import guru.nidi.graphviz.engine.Format;
import guru.nidi.graphviz.engine.Graphviz;
import guru.nidi.graphviz.engine.GraphvizV8Engine;
import guru.nidi.graphviz.model.MutableGraph;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static guru.nidi.graphviz.model.Factory.*;

/**
 * This class implements the Automaton data structure (Dictionaries of states
 * and edges)
 * 
 * @author Rafael
 *
 */

public class Automaton {
	private Map<Integer, State> states;
	private Map<Integer, Edge> edges;
	private int stateId; // Incremental value to identify states. Default 0
	private int edgeId; // Incremental value to identify edges. Default 0

	/**
	 * Constructor that initializes an empty automata
	 */
	public Automaton() {
		states = new TreeMap<Integer, State>();
		edges = new TreeMap<Integer, Edge>();
	}

	public boolean isEmpty() {
		return states.isEmpty();
	}

	public State getState(int stateId) {
		return states.get(stateId);
	}

	public Collection<State> getAllStates() {
		return states.values();
	}

	/**
	 * Method that returns the edge that match with the id of the input argument
	 * 
	 * @param edgeId id of the edge
	 * @return the edge with the given id or null otherwise
	 */
	public Edge getEdge(int edgeId) {
		return edges.get(edgeId);
	}

	public Collection<Edge> getAllEdges() {
		return edges.values();
	}

	/**
	 * Method to add a new state in the automata given the observation variables
	 * 
	 * @param vars variables of the current observation
	 * @return the new state
	 */
	public State addState(ArrayList<String> vars) {
		var newState = new State(stateId, vars);
		states.put(stateId, newState);
		stateId++;
		return newState;
	}
	public State addState(ArrayList<String> vars, int id) {
		var newState = new State(id, vars);
		states.put(id, newState);
		//stateId++;
		return newState;
	}


	/**
	 * Method to add a new Edge in the automata given the source and target states,
	 * the time passed in the source state and the event ocurred
	 * 
	 * @param sourceState
	 * @param targetState
	 * @param timeDelta
	 * @param event
	 * @return the new edge
	 */
	public Edge addEdge(State sourceState, State targetState, long timeDelta, String event) {
		var newEdge = new Edge(edgeId, sourceState.getId(), targetState.getId(), timeDelta, event);
		sourceState.addOutEdge(edgeId);
		targetState.addInEdge(edgeId);
		edges.put(edgeId, newEdge);
		edgeId++;
		// checkConsistency(sourceState);
		return newEdge;
	}

	public Edge addEdge(State sourceState, State targetState, long min, long max, String event) {
		var newEdge = new Edge(edgeId, sourceState.getId(), targetState.getId(), min, max, event);
		sourceState.addOutEdge(edgeId);
		targetState.addInEdge(edgeId);
		edges.put(edgeId, newEdge);
		edgeId++;
		// checkConsistency(sourceState);
		return newEdge;
	}

	public void deleteState(int stateId) {
		states.remove(stateId);

	}

	public void deleteEdge(int edgeId) {
		edges.remove(edgeId);
	}

	public State searchStateFromSource(State sourceState, String event, ArrayList<String> vars) {
		Optional<State> searchedState = sourceState.getOutEdges().stream().parallel().filter(indexEdge -> {
			var edge = edges.get(indexEdge);
			if (edge.getEvent().equals(event)) {
				var targetState = states.get(edge.getTargetId());
				return targetState.getAttrs().equals(vars);
			}
			return false;
		}).map(indexEdge -> states.get(edges.get(indexEdge).getTargetId())).findFirst();
		return searchedState.orElse(null);
	}

	public Optional<Edge> searchEdge(State sourceState, State nextState, String event) {
        return sourceState.getOutEdges().stream().parallel()
				.map(indexEdge -> edges.get(indexEdge)).filter(e -> {
					return e.getTargetId() == nextState.getId() && e.getEvent().equals(event);
				}).findFirst();
	}

	public void updateGuard(State sourceState, State nextState, long timeDelta, String event) {
		Optional<Edge> searchedEdge = searchEdge(sourceState, nextState, event);
		if (searchedEdge.isPresent()) {
			var edge = searchedEdge.get();

			long min = Math.min(edge.getMin(), timeDelta);
			long max = Math.max(edge.getMax(), timeDelta);

			edge.setMin(min);
			edge.setMax(max);
		}
	}

	/**
	 * Method that returns the automaton representation in Graphviz format
	 * 
	 * @return a mutableGraph
	 */

	public void exportPNG(Path path) {
		var g = toVisualize();

		try {
			Path newFilePath = Paths.get(path.toString(), ".png");
			Files.createFile(newFilePath);
			File file = newFilePath.toFile();

			Graphviz.useEngine(new GraphvizV8Engine());

			Graphviz.fromGraph(g).render(Format.DOT).toFile(file);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void exportDOT(Path path) {
		var g = toGraphvizFormat();

		try {
			Path newFilePath = Paths.get(path.toString() + ".dot");
			if (Files.exists(newFilePath)) {
				Files.delete(newFilePath);
			}
			Files.createFile(newFilePath);
			File file = newFilePath.toFile();

			Graphviz.useEngine(new GraphvizV8Engine());

			Graphviz.fromGraph(g).render(Format.DOT).toFile(file);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static Automaton importFromDOT(String route) {
		Path path = Path.of(route);
		Automaton a = new Automaton();
		try (Stream<String> lines = Files.lines(path)) {
			Pattern stateLinePat = Pattern.compile("\"\\d\" \\[.+\\]");
			Pattern edgeLinePat = Pattern.compile("\"\\d\" -> \"\\d\" \\[.+\\]");
			lines.forEach(line -> {
				String lineAux = line.trim();
				Matcher stateLineMatch = stateLinePat.matcher(lineAux);
				Matcher edgeLineMatch = edgeLinePat.matcher(lineAux);
					if(stateLineMatch.matches()) {  // It is a state
						//System.out.println("Estado "+lineAux);
						Pattern attrPat = Pattern.compile("\\[[a-zA-Z,]+\\]");
						Matcher attrsMatch = attrPat.matcher(lineAux);
						ArrayList<String> attrs = new ArrayList<String>();
						if(attrsMatch.find()) {  // The state has system attributes
							//System.out.println(matAttrs.group());
							String rawAttrs = attrsMatch.group().replace("[", "").replace("]", "");
							attrs.addAll(Arrays.asList(rawAttrs.split(",")));
							//System.out.println(attrs);
						}
						
						// Add state
						a.addState(attrs);
					} else if (edgeLineMatch.matches()) {  // Is is an edge
						// Extract states id
						Pattern edgePat = Pattern.compile("\"\\d\" -> \"\\d\"");
						Matcher edgeMatch = edgePat.matcher(lineAux);
						edgeMatch.find();
						String rawStates = edgeMatch.group().replace("\"", "");
						String[] states = rawStates.split(" -> ");
						int sourceId = Integer.parseInt(states[0]);
						int targetId = Integer.parseInt(states[1]);
						//System.out.println(edges[0]);
						//System.out.println(edges[1]);
						
						// Extract event
						String event = "";
						Pattern eventPat = Pattern.compile("\\{[a-zA-Z]+([?!])\\}");
						Matcher eventMatch = eventPat.matcher(lineAux);
						
						if(eventMatch.find()) {  // The edge has an event
							event = eventMatch.group().replace("{", "").replace("}", "");
							//System.out.println(event);
						}
						
						// Extract guard
						Pattern guardPat = Pattern.compile("[0-9]+\\, [0-9]+");
						Matcher guardMatch = guardPat.matcher(lineAux);
						guardMatch.find();
						String rawGuard = guardMatch.group().replace(" ", "");
						String[] guard = rawGuard.split(",");
						long g1 = Long.parseLong(guard[0]);
						long g2 = Long.parseLong(guard[1]);
						
						// Add edge
						a.addEdge(a.getState(sourceId), a.getState(targetId), g1, g2, event);
					}
			});
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return a;
	}
	
	private MutableGraph toGraphvizFormat() {
		return mutGraph("Automaton").setDirected(true).use((gr, ctx) -> {
			states.values().stream().forEach(state -> {
				var id = String.valueOf(state.getId());
				var variables = state.getAttrs().toString();
				var node = mutNode(id).add(Label.of(id + " " + variables));
			});
			edges.values().stream()
					.forEach(edge -> mutNode(String.valueOf(edge.getSourceId()))
							.addLink(to(mutNode(String.valueOf(edge.getTargetId()))).with(Style.DOTTED,
									Label.of("{" + edge.getEvent() + "}" + " " + edge.getGuard().toString()))));
		});
	}
	
	private MutableGraph toVisualize() {
		return mutGraph("Automaton").setDirected(true).use((gr, ctx) -> {
			states.values().stream().forEach(state -> {
				var id = String.valueOf(state.getId());
				var variables = state.getAttrs().toString();
				var node = mutNode(id).add(Label.of(id + "\n" + variables));
				if (id.equals("0"))
					node.add(Color.RED);
			});
			edges.values().stream()
					.forEach(edge -> mutNode(String.valueOf(edge.getSourceId()))
							.addLink(to(mutNode(String.valueOf(edge.getTargetId()))).with(Style.DOTTED,
									Label.of("                                    \n" + edge.getEvent() + "\n"
											+ edge.getGuard().toString() + "\n"
											+ "                                    \n"))));
		});
	}

	/**
	 * Method that displays the automata representation in a browser
	 */
	public void show() {
		var g = toVisualize();
		// g.graphAttrs().add(GraphAttr.CENTER);
		try {
			Path tempFilePathSVG = Files.createTempFile("automaton", ".svg");

			File tempFileSVG = tempFilePathSVG.toFile();

			Graphviz.useEngine(new GraphvizV8Engine());

			System.setProperty("java.awt.headless", "false");

			Graphviz.fromGraph(g).totalMemory(500_000_000).render(Format.SVG).toFile(tempFileSVG);

			String html = "<!DOCTYPE html>\n" + "<html>\n" + "    <body>\n" + "     	<object data=\"file:\\\\"
					+ tempFilePathSVG.toAbsolutePath() + "\" type=\"image/svg+xml\"></object>\n" + "    </body>\n"
					+ "</html>\n";
			Path file = Files.createTempFile("visualization", ".html");
			try {
				Files.write(file, html.getBytes());
				Desktop.getDesktop().browse(file.toUri());
			} catch (IOException e) {
				e.printStackTrace();
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Method that prints the automata in console
	 */
	public void print() {
		String output = "\n ############### States ############### \n";
		output += states.values().stream().map(Object::toString).collect(Collectors.joining("\n"));
		output += "\n ############### Edges ############### \n";
		output += edges.values().stream().map(Object::toString).collect(Collectors.joining("\n"));
		System.out.println(output);
	}

}
