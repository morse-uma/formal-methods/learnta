package automaton;

import java.util.ArrayList;
import java.util.List;

public class Edge {
	private int id;
	private int sourceId;
	private int targetId;
	private List<Long> guard;
	private String event;
	
	
	
	public Edge(int id, int sourceId, int targetId, long timeDelta, String event) {
		this.id = id;
		this.sourceId = sourceId;
		this.targetId = targetId;
		guard = new ArrayList<>(2);
		guard.add(timeDelta);
		guard.add(timeDelta); //The same initial value to match with the interval format
		this.event = event;
	}
	
	public Edge(int id, int sourceId, int targetId, long min, long max, String event) {
		this.id = id;
		this.sourceId = sourceId;
		this.targetId = targetId;
		guard = new ArrayList<>(2);
		guard.add(min);
		guard.add(max); //The same initial value to match with the interval format
		this.event = event;
	}

	public int getSourceId() {
		return sourceId;
	}

	public void setSourceId(int sourceId) {
		this.sourceId = sourceId;
	}

	public int getTargetId() {
		return targetId;
	}

	public void setTargetId(int targetId) {
		this.targetId = targetId;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public List<Long> getGuard() {
		return guard;
	}
	
	public Long getMin() {
		return guard.get(0);
	}
	
	public Long getMax() {
		return guard.get(1);
	}

	public void setGuard(List<Long> guard) {
		this.guard = guard;
	}

	public String getEvent() {
		return event;
	}

	public void setEvent(String event) {
		this.event = event;
	}
	
	public void setMax(long newMax) {
		guard.set(1, newMax);	
	}
	
	public void setMin(long newMin) {
		guard.set(0, newMin);
	}

	@Override
	public String toString() {
		return "Edge " + id + " [ sourceStateId=" + sourceId + ", targetStateId=" + targetId
				+ ", interval=" + guard.toString() + ", event=" + event + " ]";
	}
	
}
