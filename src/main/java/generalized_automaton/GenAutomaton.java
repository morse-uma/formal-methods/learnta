package generalized_automaton;

import guru.nidi.graphviz.attribute.Color;
import guru.nidi.graphviz.attribute.Label;
import guru.nidi.graphviz.attribute.Shape;
import guru.nidi.graphviz.attribute.Style;
import guru.nidi.graphviz.engine.Format;
import guru.nidi.graphviz.engine.Graphviz;
import guru.nidi.graphviz.engine.GraphvizV8Engine;
import guru.nidi.graphviz.model.MutableGraph;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.TreeMap;

import static guru.nidi.graphviz.model.Factory.*;

/**
 * This class implements the Automaton data structure (Dictionaries of states
 * and edges)
 * 
 * @author Rafael
 *
 */

public class GenAutomaton {
	private Map<Integer, GenState> states;
	private Map<Integer, GenEdge> edges;

	/**
	 * Constructor that initializes an empty automata
	 */
	public GenAutomaton() {
		states = new TreeMap<Integer, GenState>();
		edges = new TreeMap<Integer, GenEdge>();
	}
	
	public boolean isEmpty() {
		return states.isEmpty();
	}
	
	public GenState getState(int stateId) {
		return states.get(stateId);
	}

	public Collection<GenState> getAllStates() {
		return states.values();
	}
	
	/**
	 * Method that returns the edge that match with the id of the input argument
	 * 
	 * @param edgeId of the edge
	 * @return the edge with the given id or null otherwise
	 */
	public GenEdge getEdge(int edgeId) {
		return edges.get(edgeId);
	}
	
	public Collection<GenEdge> getAllEdges() {
		return edges.values();
	}

	/**
	 * Method to add a new state in the automata given the observation variables
	 * @param outEdges of the current observation
	 * @return the new state
	 */
	public GenState addState(int id, ArrayList<String> vars, ArrayList<Integer> inEdges, ArrayList<Integer> outEdges, String invariant) {
		var newState = new GenState(id, vars, inEdges, outEdges, invariant);
		states.put(id, newState);
		return newState;
	}
	
	/**
	 * Method to add a new Edge in the automata given the source and target states, the time passed in the source state and the event ocurred
	 * 
	 * @param id state, target state, time delta and event.
	 * @return the new edge
	 */
	public GenEdge addEdge(int id, Integer idQo, Integer idQt, String guard, String event) {
		var newEdge = new GenEdge(id, idQo, idQt, guard, event);
		edges.put(id, newEdge);
		
		return newEdge;
	}
	
	public void deleteState(int stateId) {
		states.remove(stateId);
	}

	public void deleteEdge(int edgeId) {
		edges.remove(edgeId);
	}
	
	private MutableGraph toVisualize() {
		return mutGraph("Automaton").setDirected(true).use((gr, ctx) -> {
			states.values().stream().forEach(state -> {
				var id = String.valueOf(state.getId());
				var attrs = state.getAttrs().toString();
				var invariant = state.getInvariant();
				var node = mutNode(id).add(Label.of(id + "\n" + attrs + "\n" + invariant));
				if (id.equals("0"))
					node.add(Color.RED);
				if (state.getOutEdges().isEmpty())
					node.add(Color.RED).add(Shape.DOUBLE_CIRCLE);
			});
			edges.values().stream()
					.forEach(edge -> mutNode(String.valueOf(edge.getSourceId()))
							.addLink(to(mutNode(String.valueOf(edge.getTargetId()))).with(Style.DOTTED,
									Label.of("                                    \n"
											+ edge.getEvent() + 
											"\n" 
											+ edge.getGuard().toString() + "\n"
											+ "                                    \n"))));
		});
	}
	
	/**
	 * Method that displays the automata representation in a browser
	 */
	public void show() {
		var g = toVisualize();
		//g.graphAttrs().add(GraphAttr.CENTER);
		try {
			Path tempFilePathSVG = Files.createTempFile("automaton", ".svg");

			File tempFileSVG = tempFilePathSVG.toFile();
			
			Graphviz.useEngine(new GraphvizV8Engine());

			System.setProperty("java.awt.headless", "false");
			
			Graphviz.fromGraph(g).totalMemory(500_000_000).render(Format.SVG).toFile(tempFileSVG);

			String html = "<!DOCTYPE html>\n" +
					  "<html>\n" +
		              "    <body>\n" +
		              "     	<object data=\"file:\\\\" + tempFilePathSVG.toAbsolutePath() + "\" type=\"image/svg+xml\"></object>\n" +
		              "    </body>\n" +
		              "</html>\n";
			Path file = Files.createTempFile("visualization", ".html");
			try {
			    Files.write(file, html.getBytes());
			    Desktop.getDesktop().browse(file.toUri());
			} catch (IOException e) {
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
