package generalized_automaton;


public class GenEdge {
	private int id;
	private int sourceId;
	private int targetId;
	private String guard;
	private String event;
	
	
	
	public GenEdge(int id, int sourceId, int targetId, String guard, String event) {
		this.id = id;
		this.sourceId = sourceId;
		this.targetId = targetId;
		this.guard = guard;
		this.event = event;
	}

	public int getSourceId() {
		return sourceId;
	}

	public void setSourceId(int sourceId) {
		this.sourceId = sourceId;
	}

	public int getTargetId() {
		return targetId;
	}

	public void setTargetId(int targetId) {
		this.targetId = targetId;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getGuard() {
		return guard;
	}

	public void setGuard(String guard) {
		this.guard = guard;
	}

	public String getEvent() {
		return event;
	}

	public void setEvent(String event) {
		this.event = event;
	}

	@Override
	public String toString() {
		return "Edge " + id + " [ sourceStateId=" + sourceId + ", targetStateId=" + targetId
				+ ", guard=" + guard.toString() + ", event=" + event + " ]";
	}
	
}
