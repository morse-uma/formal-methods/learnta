package generalized_automaton;

import java.util.ArrayList;
import java.util.List;

public class GenState {
	private int id;
	private ArrayList<String> attrs;
	private ArrayList<Integer> inEdges;
	private ArrayList<Integer> outEdges;
	private String invariant;
	
	public GenState(int id, ArrayList<String> attrs, ArrayList<Integer> inEdges, ArrayList<Integer> outEdges, String invariant) {
		this.id = id;
		this.attrs = attrs;
		this.inEdges = inEdges;
		this.outEdges = outEdges;
		this.invariant = invariant;
	}
	
	public int getId() {
		return id;
	}
	
	public List<String> getAttrs() {
		return attrs;
	}
	
	public void setAttrs(ArrayList<String> attrs) {
		this.attrs = attrs;
	}
	
	public List<Integer> getInEdges() {
		return inEdges;
	}
	
	public void setInEdges(ArrayList<Integer> inEdges) {
		this.inEdges = inEdges;
	}
	
	public List<Integer> getOutEdges() {
		return outEdges;
	}
	
	public void setOutEdges(ArrayList<Integer> outEdges) {
		this.outEdges = outEdges;
	}

	public void addOutEdge(int idE) {
		outEdges.add(idE);	
	}

	public void addInEdge(int idE) {
		inEdges.add(idE);
	}
	
	public String getInvariant() {
		return invariant;
	}

	@Override
	public String toString() {
		String output = "";
		if (id == 0) output += "Initial state 0 ";
		else output += "State " + id ;
		output += " [ attributes=" + attrs.toString() + ", ";
		output += "Out edges id " + outEdges.toString();
		output += " ]";
		return output;
	}	
	
}
