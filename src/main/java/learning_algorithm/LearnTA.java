package learning_algorithm;

import automaton.Automaton;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.json.JsonMapper;
import com.fasterxml.jackson.module.blackbird.BlackbirdModule;

import automaton.Edge;
import automaton.State;
import generalized_automaton.GenAutomaton;
import trace.Observation;
import trace.Trace;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.*;
import java.util.stream.Collectors;

public class LearnTA {
    private List<Trace> traces;
    private Automaton automaton;
    private final int k;
    boolean initVars; // Flag used to indicate that the variables of the initial state are correct

    /**
     * Default constructor of the learning algorithm class
     *
     * @return LearningAlgorithm instance
     */
    public LearnTA(String src, int k) {
        this.k = k;
        File resourcesFile = new File(src);
        var traces = readTraces(resourcesFile.getAbsoluteFile());
        learn(traces);
    }

    public LearnTA(ArrayList<Trace> traces, int k) {
        this.k = k;
        learn(traces);
    }

	/*public LearningAlgorithm(int k) {
		this.k = k;
		initVars = false;
		// learn();
	}*/

    private void learn(ArrayList<Trace> traces) {
        initVars = false;
        this.traces = compressTraces(traces);
        phase1();
        phase2();
    }

	/*public void learnNewTraces(ArrayList<Trace> traces) {
		this.traces = compressTraces(traces);
		phase1();
	}*/

	/*public void stopLearning() {
		phase2();
	}*/

    /**
     * Method to read traces from JSON
     *
     * @param source source, file in JSON format where the traces are stored
     * @return A list of the processed traces
     */
    public static ArrayList<Trace> readTraces(File source) {

        try {
            ObjectMapper mapper = JsonMapper.builder().addModule(new BlackbirdModule()).build()
                    .enable(SerializationFeature.INDENT_OUTPUT)
                    .enable(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY);

            return mapper.readValue(source, new TypeReference<ArrayList<Trace>>() {
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Method to perform a compressing operation in the input traces. The
     * compression operations are: 1. If consecutive observations havent an event
     * and all have the same variable parameters, then they are fused as one that
     * compress all the information. 2. If there is an event observation and the
     * observations that come consecutively later haven´t an event and they have the
     * same variables as the one with the event then the same fusion operation is
     * performed.
     *
     * @return traces
     */
    public static ArrayList<Trace> compressTraces(ArrayList<Trace> traces) {
        for (Trace trace : traces) {
            var obs = trace.getObs();
            ArrayList<Observation> newObs = new ArrayList<Observation>();
            for (int i = 0; i < obs.size(); i++) {
                var currentOb = obs.get(i);
                Observation lastObEq = null;
                int j = i + 1;
                while (j < obs.size() && lastObEq == null) {
                    var futOb = obs.get(j);
                    if (!checkEqOb(currentOb, futOb)) {
                        lastObEq = obs.get(j - 1);
                    } else {
                        j++;
                    }
                }

                newObs.add(currentOb);
                i = j - 1; // Next loop iteration will start with i = j (First different Observation)
            }
            trace.setObs(newObs);
        }
        return traces;
    }

    /**
     * Method to compare two obsevations
     *
     * @param ob1
     * @param ob2
     * @return true if both observations are equivalent, false otherwise
     */
    private static boolean checkEqOb(Observation ob1, Observation ob2) {
        var currentEvent = ob1.event();
        var futureEvent = ob2.event();
        if ((currentEvent.isEmpty() && currentEvent.equals(futureEvent)) || // Both observation haven´t an event
                (!currentEvent.isEmpty() && futureEvent.isEmpty())) { // First observation has an event but second not
            var currentVariables = ob1.variables();
            var futureVariables = ob2.variables();
            return currentVariables.size() == futureVariables.size() // Both Observations have the same variables
                    && currentVariables.containsAll(futureVariables);
        }
        return false;
    }

    /**
     * Construct the barebone of the automata
     */
    private void phase1() {
        if (automaton == null)
            automaton = new Automaton();

        for (var trace : traces) {
            processTrace(trace);
        }
    }

    private void processTrace(Trace trace) {
        var obs = trace.getObs();
        var ob = obs.get(0);
        long currentTime = ob.time();
        long lastTime = 0;
        int i = 1;
        State qo = automaton.isEmpty() ? null : automaton.getState(0); // Last visited state
        State qt = null; // Target state from qo
        if (qo == null) { // If the automata is empty, create the first state
            qo = automaton.addState(ob.variables());
            if (ob.event().isEmpty()) {
                initVars = true;
            } else { // If there is an event in the initial state and it is the first trace, two
                // states are considered in the
                // initial observation
                qt = automaton.addState(ob.variables());
                long delta = getDelta(currentTime, lastTime);
                automaton.addEdge(qo, qt, delta, ob.event());
                qo = qt;
            }
        } else { // In an existing automata, new traces start in the first state
            if (ob.event().isEmpty()) {
                /*
                 * Check if variables are equal. If not, trace without event will have priority
                 * to correct the variables asumption from another trace starting with an event
                 * (both initial state and its consecutive will have the same variables).
                 *
                 * If there was a previous observation without event in the initial state and
                 * the initial state variables differ with the current observation, a
                 * consistency error is raised
                 *
                 */
                if (initVars && !qo.getAttrs().equals(ob.variables())) {
                    throw new RuntimeException(qo.toString() + "\n" + "Have an inconsistency between variables \n"
                            + "Current variables: " + qo.getAttrs().toString() + "\n" + "Observation variables: "
                            + ob.variables().toString());
                } else if (!initVars) {
                    qo.setAttrs(ob.variables());
                    initVars = true;
                }
                qt = qo;
            } else { // If there is an event in the initial ob, two states are considered
                qt = automaton.searchStateFromSource(qo, ob.event(), ob.variables());

                if (qt != null) { // If there is an existing state, update the guards
                    long delta = 0;
                    automaton.updateGuard(qo, qt, delta, ob.event());
                } else { // In other case, create a new state and an edge
                    // k + 1 because arrays skip last explicit index
                    // int futureIdx = Math.min(0 + (k + 1),trace.getObs().size());
                    qt = Math.min((k + 1), trace.getObs().size()) == (k + 1)
                            ? fastMatching(trace.getObs().subList(0, k + 1), qo, lastTime)
                            : null;
                    if (qt != null) {
                        i += k;
                    } else {
                        qt = automaton.addState(ob.variables());
                        long delta = getDelta(currentTime, lastTime);
                        automaton.addEdge(qo, qt, delta, ob.event());
                    }
                }
            }
            lastTime = currentTime;
            qo = qt;
        }

        while (i < trace.getObs().size()) {
            ob = obs.get(i);
            qt = automaton.searchStateFromSource(qo, ob.event(), ob.variables());

            if (qt != null) { // If there is an existing state, update the guards
                currentTime = ob.time();
                long delta = getDelta(currentTime, lastTime);
                automaton.updateGuard(qo, qt, delta, ob.event());
            } else { // In other case, create a new state and an edge
                // k + 1 because arrays skip last explicit index
                int futureIdx = Math.min(i + (k + 1), trace.getObs().size());
                qt = (futureIdx - i) == (k + 1) ? fastMatching(trace.getObs().subList(i, futureIdx), qo, lastTime) : null;
                if (qt != null) {
                    i += k;
                    ob = obs.get(i);
                    currentTime = ob.time();
                } else {
                    qt = automaton.addState(ob.variables());
                    currentTime = ob.time();
                    long delta = getDelta(currentTime, lastTime);
                    automaton.addEdge(qo, qt, delta, ob.event());
                }
            }
            qo = qt;
            lastTime = currentTime;
            i += 1;
        }
    }

    private long getDelta(long t1, long t2) {
        var delta = t1 - t2;
        if (delta <= 0)
            throw new RuntimeException("There is not elapse time between observations");
        return delta;
    }

    /**
     * Method that tries to compute a merge operation given the last observation and
     * the future k observations, this merge "on the fly" tries to reduce the space
     * and time cost of the learning process. The method compares the observation
     * and all existing states in the automaton in order to perform a "equivalence
     * merge operation" with the first k future equal state found (only perform the
     * merge operation between two states in each call).
     *
     * @param obsWindow (curent observation and its k future observations)
     * @param qo        Last visited state
     * @param lastTime
     * @return Last state in fast merge or null if can not be performed
     */
    private State fastMatching(List<Observation> obsWindow, State qo, long lastTime) {
        // List of all states that are possible candidates to perform a merge operation
        ArrayList<State> pEqStates = automaton.getAllStates().stream().filter(state -> {
            var vars = state.getAttrs();
            return obsWindow.get(0).variables().equals(vars);
        }).collect(Collectors.toCollection(ArrayList::new));

        if (pEqStates.isEmpty())
            return null;

        // Build the k future of the current observation given an observation window of
        // length k
        ArrayList<Object> obFut = new ArrayList<>(); // Future of the observation
        Iterator<Observation> it = obsWindow.iterator();
        var lastObservation = it.next(); // Skip current observation to construct its future
        long delta = getDelta(lastObservation.time(), lastTime);
        long auxLastTime = lastObservation.time();
        while (it.hasNext()) {
            lastObservation = it.next();
            long auxTimeDelta = getDelta(lastObservation.time(), auxLastTime);
            auxLastTime = lastObservation.time();
            var edge = new Edge(-1, -1, -1, auxTimeDelta, lastObservation.event());
            var state = new State(-1, lastObservation.variables());
            obFut.add(edge);
            obFut.add(state);
        }

        // Loop through all candidates to try to perform a merge operation
        for (State currentEqState : pEqStates) {
            var futuresOfCandidate = getKFutures(currentEqState);

            // Search for a future that is equal to the observation future
            Optional<ArrayList<Object>> sameFuture = futuresOfCandidate.stream().filter(future -> {
                return fmComparison(obFut, future);
            }).findFirst();

            if (sameFuture.isPresent()) { // If there is an equivalent future
                automaton.addEdge(qo, currentEqState, delta, obsWindow.get(0).event());
                Iterator<Object> itSameFuture = sameFuture.get().iterator();
                Iterator<Object> itObservationFuture = obFut.iterator();
                while (itSameFuture.hasNext()) {
                    var stateOrEdge1 = itSameFuture.next();
                    var stateOrEdge2 = itObservationFuture.next();
                    if (stateOrEdge1 instanceof Edge edge1 && stateOrEdge2 instanceof Edge edge2) {
                        // Update the guards of every edge in the equivalent future given the source and
                        // the target states and the timedelta of the current observation k futures
                        automaton.updateGuard(automaton.getState(edge1.getSourceId()),
                                automaton.getState(edge1.getTargetId()), edge2.getGuard().get(0), edge1.getEvent());
                    } else {
                        qo = (State) stateOrEdge1;
                    }
                }
                return qo; // Return the new merged state
            }
        }
        return null;
    }

    /**
     * Method to obtain the k futures of a given state
     *
     * @param qo
     * @return futures, a list of all posible future paths with the form [edge,
     * state, edge...state]. For example: 3 Futures (future of depth 3) of
     * "S0" could be [[edge0, S1, edge2, S2, edge4, S4],[edge1, S2, edge3,
     * S3, edge5, S5]]
     */
    private ArrayList<ArrayList<Object>> getKFutures(State qo) {
        ArrayList<ArrayList<Object>> futures = new ArrayList<>();

        for (int idEdge : qo.getOutEdges()) {
            int idQt = automaton.getEdge(idEdge).getTargetId();
            var edge = automaton.getEdge(idEdge);
            var qt = automaton.getState(idQt);
            futures.addAll(getKFuturesAux(new ArrayList<Object>(Arrays.asList(edge, qt)), 2));
        }
        return futures;
    }

    /**
     * Auxiliar method to perform recursion in order to discover all posible k
     * future path
     *
     * @param currentPath, Current future path
     * @param level,       level of recursion
     * @return futures, a list of all posible future paths with the form [edge,
     * state, edge...state]. For example: 3 Futures (future of depth 3) of
     * "S0" could be [[edge0, S1, edge2, S2, edge4, S4],[edge1, S2, edge3,
     * S3, edge5, S5]]
     */
    private ArrayList<ArrayList<Object>> getKFuturesAux(ArrayList<Object> currentPath, int level) {
        ArrayList<ArrayList<Object>> futures = new ArrayList<>();
        if (level <= k) {
            if (((State) currentPath.get(currentPath.size() - 1)).getOutEdges().isEmpty()) {
                futures.add(currentPath);
            } else {
                for (int idEdge : ((State) currentPath.get(currentPath.size() - 1)).getOutEdges()) {
                    int idOutState = automaton.getEdge(idEdge).getTargetId();
                    var edge = automaton.getEdge(idEdge);
                    var outState = automaton.getState(idOutState);
                    var newPath = new ArrayList<Object>(currentPath);
                    newPath.add(edge);
                    newPath.add(outState);
                    futures.addAll(getKFuturesAux(newPath, level + 1));
                }
            }
        } else {
            futures.add(currentPath);
        }
        return futures;
    }

    /**
     * Method that performs a comparison between two future paths (trace and
     * automaton)
     *
     * @param future1
     * @param future2
     * @return true if both futures are equivalent (same events, variables and
     * length), false otherwise
     */
    private boolean fmComparison(ArrayList<Object> future1, ArrayList<Object> future2) {
        var size1 = future1.size();
        var size2 = future2.size();
        if (size1 == size2) {
            Iterator<Object> it2 = future2.iterator();

            return future1.stream().allMatch(stateOrEdge1 -> {
                var stateOrEdge2 = it2.next();
                if (stateOrEdge1 instanceof State state1 && stateOrEdge2 instanceof State state2) {

                    return state1.getAttrs().equals(state2.getAttrs());

                } else if (stateOrEdge1 instanceof Edge edge1 && stateOrEdge2 instanceof Edge edge2) {
                    return edge1.getEvent().equals(edge2.getEvent());
                }
                return false;
            });
        }
        return false;
    }

    private void phase2() {
        boolean merged = true;
        boolean indet;
        boolean fixed = true;
        do {
            while (merged) {
                merged = lookForMerge();
            }
            var indetEdges = indetEdges();
            indet = indetEdges != null;
            if (indet) {
                fixed = fixIndet(indetEdges);
                if (fixed)
                    merged = true;
            }
        } while (merged);
        if (!fixed) {
            automaton.show();
            throw new RuntimeException("indeterministic automata");
        }
        merged = true;
        while (merged) {
            merged = mergeFinalStates();
        }
    }

    /**
     * Method used to search inconsistencies in the out edges of every state.
     * Inconsistencies checked
     * 1.If there are two or more edges with the same event, overlapping guards and the target state with the same attrs.
     *
     * @return indeterministic edges or null otherwise
     */
    private ArrayList<Edge> indetEdges() {
        var states = automaton.getAllStates();

        for (State state : states) {
            for (var idEdge : state.getOutEdges()) {
                var edge = automaton.getEdge(idEdge);
                var event = edge.getEvent();
                var dupEdges = state.getOutEdges().stream().map(automaton::getEdge).filter(e -> {  // Check for inconsistencies
                    if (e.getId() == edge.getId())
                        return false;
                    if (e.getEvent().equals(event)) {
                        boolean sameTarget = e.getTargetId() == edge.getTargetId();  // Same target state
                        boolean sameAttrs = automaton.getState(e.getTargetId()).getAttrs().equals(automaton.getState(edge.getTargetId()).getAttrs());
                        var minVal1 = edge.getMin();
                        var minVal2 = e.getMin();
                        var maxVal1 = edge.getMax();
                        var maxVal2 = e.getMax();
                        boolean overlappingGuards = (minVal1 >= minVal2 && minVal1 <= maxVal2)  // Overlapping guards
                                || (maxVal1 >= minVal2 && maxVal1 <= maxVal2);
                        return (sameTarget | sameAttrs) && overlappingGuards;  // If two edges have overlapping guards and the same target state there is an inconsistency
                    }
                    return false;
                }).collect(Collectors.toCollection(ArrayList::new));
                if (!dupEdges.isEmpty()) {
                    dupEdges.add(edge);
                    return dupEdges;
                }
            }
        }
        return null;
    }

    /**
     * Method used to merge the target states of the indeterministic edges if
     * possible All edges have the same source state.
     *
     * @param indetEdges
     * @return boolean
     */
    private boolean fixIndet(ArrayList<Edge> indetEdges) {
        var variables = automaton.getState(indetEdges.get(0).getTargetId()).getAttrs();
        var eqStates = indetEdges.stream().map(e -> automaton.getState(e.getTargetId()))
                .filter(s -> s.getAttrs().equals(variables)).distinct()
                .collect(Collectors.toCollection(ArrayList::new));
        if (eqStates.size() > 1) {
            var newState = eqStates.get(0);
            for (int i = 1; i < eqStates.size(); i++) {
                newState = mergeStates(newState, eqStates.get(i));
                mergeEdges(newState);
            }
            return true;
        }
        return false;
    }

    private boolean lookForMerge() {
        var states = automaton.getAllStates();

        for (State state : states) {
            var kFutures1 = getKFutures(state);
            var possibleEquivalentState = states.stream().filter(state2 -> {
                if (state.getAttrs().equals(state2.getAttrs()) && !state.equals(state2)) {
                    var kFutures2 = getKFutures(state2);
                    if (!kFutures1.isEmpty() && !kFutures2.isEmpty()) {
                        return compareKFutures(kFutures1, kFutures2);
                    }
                }
                return false;
            }).findFirst();

            if (possibleEquivalentState.isPresent()) {
                var equivalentState = possibleEquivalentState.get();
                var mergedState = mergeStates(state, equivalentState);
                mergeEdges(mergedState);
                return true;
            }
        }
        return false;
    }

    /**
     * Method that performs a comparison between the k-futures of two states
     *
     * @param fs1
     * @param fs2
     * @return true if both k-futures are equivalent, false otherwise
     */
    private boolean compareKFutures(ArrayList<ArrayList<Object>> fs1, ArrayList<ArrayList<Object>> fs2) {
        var nFut1 = fs1.size();
        var nFut2 = fs2.size();

        if (nFut1 > nFut2) {
            return fs2.stream().allMatch(f2 -> {
                return fs1.stream().anyMatch(f1 -> compareFutures(f1, f2, "c2"));
            });
        } else if (nFut2 > nFut1) {
            return fs1.stream().allMatch(f1 -> {
                return fs2.stream().anyMatch(f2 -> compareFutures(f1, f2, "c2"));
            });
        } else {
            return fs2.stream().allMatch(f2 -> {
                return fs1.stream().anyMatch(f1 -> compareFutures(f1, f2, "c1"));
            });
        }
    }

    /**
     * Method that performs a comparison between two future paths
     *
     * @param f1
     * @param f2
     * @param crit
     * @return true if both futures are equivalent (same events, variables and
     * length), false otherwise
     */
    private boolean compareFutures(ArrayList<Object> f1, ArrayList<Object> f2, String crit) {
        var size1 = f1.size();
        var size2 = f2.size();
        if (size1 == size2) {
            Iterator<Object> it2 = f2.iterator();

            return f1.stream().allMatch(stateOrEdge1 -> {
                var stateOrEdge2 = it2.next();
                if (stateOrEdge1 instanceof State state1 && stateOrEdge2 instanceof State state2) { // States
                    return state1.getAttrs().equals(state2.getAttrs());

                } else if (stateOrEdge1 instanceof Edge edge1 && stateOrEdge2 instanceof Edge edge2) { // Edges
                    if (!edge1.getEvent().equals(edge2.getEvent()))
                        return false;
                    var minVal1 = edge1.getMin();
                    var minVal2 = edge2.getMin();
                    var maxVal1 = edge1.getMax();
                    var maxVal2 = edge2.getMax();
                    if (crit.equals("c1")) {
                        return (minVal1 >= minVal2 && minVal1 <= maxVal2) || (maxVal1 >= minVal2 && maxVal1 <= maxVal2);
                    } else {
                        return (minVal1 >= minVal2 && minVal1 <= maxVal2) && (maxVal1 >= minVal2 && maxVal1 <= maxVal2)
                                || (minVal2 >= minVal1 && minVal2 <= maxVal1)
                                && (maxVal2 >= minVal1 && maxVal2 <= maxVal1);
                    }
                }
                return false;
            });
        }
        return false;
    }

    /**
     * Method used to merge two equivalent states
     *
     * @param s1
     * @param s2
     * @return resulting state
     */
    private State mergeStates(State s1, State s2) {
        var stateMerged = automaton.getState(Math.min(s1.getId(), s2.getId()));
        var stateAux = automaton.getState(Math.max(s1.getId(), s2.getId()));

        stateAux.getOutEdges().stream().map(idEdge -> automaton.getEdge(idEdge)).forEach(edge -> {
            edge.setSourceId(stateMerged.getId());
            stateMerged.addOutEdge(edge.getId());
        });

        stateAux.getInEdges().stream().map(idEdge -> automaton.getEdge(idEdge)).forEach(edge -> {
            edge.setTargetId(stateMerged.getId());
            stateMerged.addInEdge(edge.getId());
        });

        automaton.deleteState(stateAux.getId());

        return stateMerged;
    }

    /**
     * This method looks for duplicate in and out edges for the input state and
     * merge them. Duplicate edges: Same source, target and event
     *
     * @param mergedState
     */
    private void mergeEdges(State mergedState) {
        var deletingEdges = new ArrayList<Integer>();

        // Checking outEdges
        Map<String, List<Edge>> possibleEquivalentOutEdges = mergedState.getOutEdges().stream()
                .map(idEdge -> automaton.getEdge(idEdge)).collect(Collectors.groupingBy(Edge::getEvent)).entrySet()
                .stream().filter(kv -> kv.getValue().size() > 1)
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
        // automata.show();
        for (List<Edge> comparingEdges : possibleEquivalentOutEdges.values()) {
            boolean merged = true;
            // ArrayList<Integer> candidates = comparingEdges.stream().map(e ->
            // e.getId()).collect(Collectors.toCollection(ArrayList::new));
            while (merged) {
                merged = false;
                int i = 0;
                ArrayList<Edge> compared = new ArrayList<>();
                while (i < comparingEdges.size() && !merged) {
                    Edge currentE = comparingEdges.get(i);
                    ArrayList<Edge> eqEdges = comparingEdges.stream().filter(e -> {
                        if (e.getId() == currentE.getId())
                            return false;
                        var minVal1 = currentE.getMin(); // 26, 26
                        var minVal2 = e.getMin(); // 21, 26
                        var maxVal1 = currentE.getMax();
                        var maxVal2 = e.getMax();
                        if (((minVal1 >= minVal2 && minVal1 <= maxVal2) || (maxVal1 >= minVal2 && maxVal1 <= maxVal2))
                                && currentE.getTargetId() == e.getTargetId())
                            return true;
                        return false;
                    }).collect(Collectors.toCollection(ArrayList::new));

                    if (!eqEdges.isEmpty()) {
                        compared.add(currentE);
                        compared.addAll(eqEdges);
                        var mergedEdge = compared.stream().min(Comparator.comparing(Edge::getId)).get();
                        var minGuard = compared.stream().min(Comparator.comparing(Edge::getMin)).get().getMin();
                        var maxGuard = compared.stream().max(Comparator.comparing(Edge::getMax)).get().getMax();
                        mergedEdge.setMin(minGuard);
                        mergedEdge.setMax(maxGuard);
                        var toDelete = compared.stream().filter(edge -> {
                            if (edge.getId() != mergedEdge.getId() && !deletingEdges.contains(edge.getId()))
                                return true;
                            return false;
                        }).map(Edge::getId).collect(Collectors.toCollection(ArrayList::new));
                        deletingEdges.addAll(toDelete);
                        merged = true;
                        compared.remove(mergedEdge);
                    }
                    i++;
                }
                if (merged) {
                    comparingEdges.removeAll(compared);
                }
            }
        }

        // Checking inEdges
        Map<String, List<Edge>> possibleEquivalentInEdges = mergedState.getInEdges().stream()
                .map(idEdge -> automaton.getEdge(idEdge)).collect(Collectors.groupingBy(Edge::getEvent)).entrySet()
                .stream().filter(kv -> kv.getValue().size() > 1)
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

        for (List<Edge> comparingEdges : possibleEquivalentInEdges.values()) {
            boolean merged = true;
            while (merged) {
                merged = false;
                int i = 0;
                ArrayList<Edge> compared = new ArrayList<>();
                while (i < comparingEdges.size() && !merged) {
                    Edge currentE = comparingEdges.get(i);
                    ArrayList<Edge> eqEdges = comparingEdges.stream().filter(e -> {
                        if (e.getId() == currentE.getId())
                            return false;
                        var minVal1 = currentE.getMin();
                        var minVal2 = e.getMin();
                        var maxVal1 = currentE.getMax();
                        var maxVal2 = e.getMax();
                        if (((minVal1 >= minVal2 && minVal1 <= maxVal2) || (maxVal1 >= minVal2 && maxVal1 <= maxVal2))
                                && currentE.getSourceId() == e.getSourceId())
                            return true;
                        return false;
                    }).collect(Collectors.toCollection(ArrayList::new));

                    if (!eqEdges.isEmpty()) {
                        compared.add(currentE);
                        compared.addAll(eqEdges);
                        var mergedEdge = compared.stream().min(Comparator.comparing(Edge::getId)).get();
                        var minGuard = compared.stream().min(Comparator.comparing(Edge::getMin)).get().getMin();
                        var maxGuard = compared.stream().max(Comparator.comparing(Edge::getMax)).get().getMax();
                        mergedEdge.setMin(minGuard);
                        mergedEdge.setMax(maxGuard);
                        var toDelete = compared.stream().filter(edge -> {
                            if (edge.getId() != mergedEdge.getId() && !deletingEdges.contains(edge.getId()))
                                return true;
                            return false;
                        }).map(Edge::getId).collect(Collectors.toCollection(ArrayList::new));
                        deletingEdges.addAll(toDelete);
                        merged = true;
                        compared.remove(mergedEdge);
                    }
                    i++;
                }
                if (merged) {
                    comparingEdges.removeAll(compared);
                }
            }
        }

        deletingEdges.stream().forEach(idEdge -> {
            var deletingEdge = automaton.getEdge(idEdge);
            var sourceState = automaton.getState(deletingEdge.getSourceId());
            var targetState = automaton.getState(deletingEdge.getTargetId());
            sourceState.getOutEdges().remove(idEdge);
            targetState.getInEdges().remove(idEdge);
            automaton.deleteEdge(idEdge);
        });
    }

    private boolean mergeFinalStates() {
        var states = automaton.getAllStates();
        var finalStates = states.stream().filter(state -> state.getOutEdges().size() == 0)
                .collect(Collectors.toCollection(ArrayList::new));

        for (State finalS : finalStates) {
            var possibleEquivalentLeave = finalStates.stream().filter(leave2 -> {
                if (!finalS.equals(leave2) && finalS.getAttrs().equals(leave2.getAttrs())) {
                    var edgesInLeave1 = finalS.getInEdges().stream().map(idEdge -> automaton.getEdge(idEdge))
                            .collect(Collectors.toCollection(ArrayList::new));
                    var edgesInLeave2 = leave2.getInEdges().stream().map(idEdge -> automaton.getEdge(idEdge))
                            .collect(Collectors.toCollection(ArrayList::new));

                    if (!edgesInLeave1.isEmpty() && !edgesInLeave2.isEmpty()) {
                        if (edgesInLeave1.size() > edgesInLeave2.size()) {
                            return edgesInLeave2.stream().allMatch(edge2 -> {
                                return edgesInLeave1.stream()
                                        .anyMatch(edge -> edge.getEvent().equals(edge2.getEvent()));
                            });
                        } else {
                            return edgesInLeave1.stream().allMatch(edge -> {
                                return edgesInLeave2.stream()
                                        .anyMatch(edge2 -> edge2.getEvent().equals(edge.getEvent()));
                            });
                        }
                    }
                }
                return false;
            }).findFirst();

            if (possibleEquivalentLeave.isPresent()) {
                var equivalentLeave = possibleEquivalentLeave.get();
                var mergedLeave = mergeStates(finalS, equivalentLeave);
                mergeEdges(mergedLeave);
                return true;
            }
        }
        return false;
    }

    public static void show(Automaton a) {
        a.show();
    }

    public static void show(GenAutomaton a) {
        a.show();
    }

    public void exportPNG(String route) {
        automaton.exportPNG(Path.of(route));
    }

    public void exportDOT(String route) {
        automaton.exportDOT(Path.of(route));
    }

    public Automaton getAutomaton() {
        return automaton;
    }

    public GenAutomaton generalize() {
        var ga = new GenAutomaton();
        automaton.getAllStates().stream().forEach(state -> {
            // ga.addState(state.getId(), state.getVariables());
            if (!state.getOutEdges().isEmpty()) {
                Edge edge = state.getOutEdges().stream().map(idEdge -> automaton.getEdge(idEdge)).max(Comparator.comparing(Edge::getMax)).get();
                String event = edge.getEvent();
                String guard = "";
                String invariant = "";
                if (event.isEmpty() || event.endsWith("?")) {
                    guard = ">=" + edge.getMin().toString();
                } else if (event.endsWith("!")) {
                    guard = ">=" + edge.getMin().toString();
                    invariant = "<=" + edge.getMax().toString();
                } else {
                    // error?
                }
                ga.addState(state.getId(), state.getAttrs(), state.getInEdges(), state.getOutEdges(), invariant);
                ga.addEdge(edge.getId(), edge.getSourceId(), edge.getTargetId(), guard, event);
                state.getOutEdges().stream().filter(id -> id != edge.getId()).map(id -> automaton.getEdge(id)).forEach(e -> {
                    ga.addEdge(e.getId(), e.getSourceId(), e.getTargetId(), "[" + e.getMin() + "," + e.getMax() + "]", e.getEvent());
                });
            } else {
                ga.addState(state.getId(), state.getAttrs(), state.getInEdges(), state.getOutEdges(), "");
            }
        });
        return ga;
    }
}
