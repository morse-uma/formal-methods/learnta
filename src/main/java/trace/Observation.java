package trace;

import java.util.ArrayList;

public record Observation(long time, String event,ArrayList<String> variables) {}