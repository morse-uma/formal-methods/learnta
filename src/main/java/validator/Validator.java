package validator;

import automaton.Automaton;
import automaton.State;
import generalized_automaton.GenAutomaton;
import generalized_automaton.GenState;
import learning_algorithm.LearnTA;
import trace.Observation;
import trace.Trace;

import java.io.File;
import java.util.ArrayList;

public class Validator {

    public static int nValidTraces(String src, Automaton a) {
        File resourcesFile = new File(src);
        var traces = LearnTA.readTraces(resourcesFile.getAbsoluteFile());

        return nValidTraces(traces, a);
    }

    public static int nValidTraces(String src, GenAutomaton ga) {
        File resourcesFile = new File(src);
        var traces = LearnTA.readTraces(resourcesFile.getAbsoluteFile());

        return nValidTraces(traces, ga);
    }

    public static int nValidTraces(ArrayList<Trace> traces, Automaton a) {
        var compTraces = LearnTA.compressTraces(traces);
        int nValid = 0;
        for (Trace t : compTraces) {
            if (checkTrace(t, a)) {
                nValid += 1;
            }
        }
        // System.out.println("Traces: " + compTraces.size());
        // System.out.println("Valid traces: " + nValid);
        return nValid;
    }


    public static int nValidTraces(ArrayList<Trace> traces, GenAutomaton ga) {
        var compTraces = LearnTA.compressTraces(traces);
        int nValid = 0;
        for (Trace t : compTraces) {
            if (checkTrace(t, ga)) {
                nValid += 1;
            }
        }
        // System.out.println("Traces: " + compTraces.size());
        // System.out.println("Valid traces: " + nValid);
        return nValid;
    }

    private static boolean checkTrace(Trace t, GenAutomaton ga) {
        GenState lastState = null;
        long lastTimeStamp = 0;
        for (Observation obs : t.getObs()) {
            String event = obs.event();
            ArrayList<String> attrs = obs.variables();
            var timeDelta = obs.time() - lastTimeStamp;
            // First state
            if (lastState == null) {
                lastState = ga.getState(0);
                // No event means that there is not a transition between states
                if (event.isEmpty()) {
                    String invariant = lastState.getInvariant();
                    boolean validTime = invariant.isEmpty() || timeDelta <= Integer.parseInt(invariant.replace("<=", "").strip());
                    boolean sameAttrs = attrs.equals(lastState.getAttrs());
                    if (!sameAttrs || !validTime) {
                        return false;
                    }
                    continue;
                }
            }

            // Theoretically there is only one possible edge, if the automaton follows the
            // rules
            var pEdge = lastState.getOutEdges().stream().map(ga::getEdge).filter(e -> {
                boolean sameEvent = e.getEvent().equals(event);
                boolean validTime = false;
				boolean targetWithSameAttrs = ga.getState(e.getTargetId()).getAttrs().equals(obs.variables());
                String guard = e.getGuard();
                if (guard.contains(">=")) {
                    validTime = Integer.parseInt(guard.replace(">=", "").strip()) <= timeDelta;
                } else {
                    String[] guardInterval = guard.replace("[", "").replace("]", "").strip().split(",");
                    int min = Integer.parseInt(guardInterval[0]);
                    int max = Integer.parseInt(guardInterval[1]);
                    validTime = timeDelta >= min && timeDelta <= max;
                }
                return sameEvent && validTime && targetWithSameAttrs;
            }).findFirst();

            if (pEdge.isEmpty()) {
                return false;
            }

            var edge = pEdge.get();
            var targetState = ga.getState(edge.getTargetId());

            lastTimeStamp = obs.time();
            lastState = targetState;
        }
        return true;
    }

    public static int nValidTracesRTI(ArrayList<Trace> traces, Automaton automaton) {
        int nValid = 0;

        for (Trace t : traces) {
            if (checkTraceRTI(t, automaton)) {
                nValid += 1;
            }
        }
        // System.out.println("Traces: " + compTraces.size());
        // System.out.println("Valid traces: " + nValid);
        return nValid;
    }

    public static boolean checkTrace(Trace t, Automaton automaton) {
        State lastState = null;
        long lastTimeStamp = 0;
        for (Observation obs : t.getObs()) {
            String event = obs.event();
            ArrayList<String> variables = obs.variables();
            var timeDelta = obs.time() - lastTimeStamp;
            // First state
            if (lastState == null) {
                lastState = automaton.getState(0);
                // No event means that there is not a transition between states
                if (event.isEmpty()) {
                    if (!variables.equals(lastState.getAttrs())) {
                        return false;
                    }
                    continue;
                }
            }

            // Theoretically there is only one possible edge, if the automaton follows the
            // rules
            var pEdge = lastState.getOutEdges().stream().map(automaton::getEdge).filter(e -> {
                return e.getEvent().equals(event) && (timeDelta >= e.getMin() && timeDelta <= e.getMax() && automaton.getState(e.getTargetId()).getAttrs().equals(obs.variables()));
            }).findFirst();

            if (pEdge.isEmpty()) {
                return false;
            }
            var edge = pEdge.get();
            var targetState = automaton.getState(edge.getTargetId());

            lastTimeStamp = obs.time();
            lastState = targetState;
        }
        return true;
    }

    public static boolean checkTraceRTI(Trace t, Automaton automaton) {
        State lastState = null;
        long lastTimeStamp = 0;
        for (int i = 0; i < t.getObs().size(); i++) {
            Observation obs = t.getObs().get(i);
            String event = obs.event().isEmpty() ? "n" : obs.event().replace("?", "").replace("!", "");
            ArrayList<String> variables = obs.variables();
            var timeDelta = obs.time() - lastTimeStamp;
            // First state
            if (lastState == null) {
                lastState = automaton.getState(0);
                // No event means that there is not a transition between states
                if (event.isEmpty()) {
                    if (!variables.equals(lastState.getAttrs())) {
                        return false;
                    }
                    continue;
                }
            }

            // Theorically there is only one possible edge, if the automaton follows the
            // rules
            var pEdge = lastState.getOutEdges().stream().map(automaton::getEdge).filter(e -> {
                return e.getEvent().equals(event) && (timeDelta >= e.getMin() && timeDelta <= e.getMax());
            }).findFirst();

            if (pEdge.isEmpty()) {
                return false;
            }
            var edge = pEdge.get();
            var targetState = automaton.getState(edge.getTargetId());
            if (!variables.equals(targetState.getAttrs())) {
                return false;
            }

            lastTimeStamp = obs.time();
            lastState = targetState;
        }
        return true;
    }
}
